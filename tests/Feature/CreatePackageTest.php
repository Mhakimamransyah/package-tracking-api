<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\ObjectId as BSONObjectId;
use Tests\TestCase;

class CreatePackageTest extends TestCase
{
    public function test_success_create(): void
    {
        $this->post('/api/v1/package', [
            "transaction" =>[
                "transaction_amount" => "70700",
                "transaction_discount" =>  "0",
                "transaction_additional_field" => "",
                "transaction_payment_type" => "29",
                "transaction_state" => "PAID",
                "transaction_payment_type_name" => "Invoice",
                "transaction_cash_amount" => 0,
                "transaction_cash_change" => 0,
                "customer_attribute" => [
                    "Nama_Sales" => "Radit Fitrawikarsa",
                    "TOP" => "14 Hari",
                    "Jenis_Pelanggan" => "B2B"
                ],
                "custom_field" => [
                    "catatan_tambahan" => "JANGAN DI BANTING / DI TINDIH"
                ],
                "currentLocation" => [
                    "name" => "Hub Jakarta Selatan",
                    "code" => "JKTS01",
                    "type" => "Agent"
                ],
                "origin_data" => "652e2c4919db60ea923c6c4d",
                "destination_data"=>"652e2c4919db60ea923c6c4e",
                "customer_id" => "652e2cc719db60ea923c6c50"
            ],
            "connote" => [
                "connote_number" => 1,
                "connote_service" => "ECO",
                "connote_service_price"=> 70700,
                "connote_amount"=> 70700,
                "connote_code"=> "AWB00100209082020",
                "connote_booking_code"=> "",
                "connote_state"=> "PAID",
                "connote_state_id"=> 2,
                "surcharge_amount"=> null,
                "actual_weight"=> 20,
                "volume_weight"=> 0,
                "chargeable_weight"=> 20,
                "organization_id"=> 6,
                "location_id"=> "5cecb20b6c49615b174c3e74",
                "connote_total_package"=> "3",
                "connote_surcharge_amount"=> "0",
                "connote_sla_day"=> "4",
                "location_name"=> "Hub Jakarta Selatan",
                "location_type"=> "HUB",
                "source_tariff_db"=> "tariff_customers",
                "id_source_tariff"=> "1576868",
                "pod"=> null,
                "history"=> [],
                "koli_data" => [
                    [
                        "koli_length"=> 0,
                        "koli_chargeable_weight"=> 9,
                        "koli_width"=> 0,
                        "koli_surcharge"=> [],
                        "koli_height"=> 0,
                        "koli_description"=> "V WARP",
                        "koli_formula_id"=> null,
                        "koli_volume"=> 0,
                        "koli_weight"=> 9,
                        "koli_custom_field"=> [
                            "awb_sicepat"=> null,
                            "harga_barang"=> null
                        ]
                    ]
                ]
            ]
        ])->assertStatus(
            Response::HTTP_CREATED
        );
    }

    public function test_bad_request(): void
    {
        $this->post('/api/v1/package', [
            "transaction" =>[
                "transaction_amount" => 0
            ],
            "connote" => [],
        ])->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_customers_not_found(): void
    {
        $this->post('/api/v1/package', [
            "transaction" =>[
                "transaction_amount" => "70700",
                "transaction_discount" =>  "0",
                "transaction_additional_field" => "",
                "transaction_payment_type" => "29",
                "transaction_state" => "PAID",
                "transaction_payment_type_name" => "Invoice",
                "transaction_cash_amount" => 0,
                "transaction_cash_change" => 0,
                "customer_attribute" => [
                    "Nama_Sales" => "Radit Fitrawikarsa",
                    "TOP" => "14 Hari",
                    "Jenis_Pelanggan" => "B2B"
                ],
                "custom_field" => [
                    "catatan_tambahan" => "JANGAN DI BANTING / DI TINDIH"
                ],
                "currentLocation" => [
                    "name" => "Hub Jakarta Selatan",
                    "code" => "JKTS01",
                    "type" => "Agent"
                ],
                "origin_data" => "652e2c4919db60ea923c6c40",
                "destination_data"=>"652e2c4919db60ea923c6c4",
                "customer_id" => "652e2cc719db60ea923c6c5"
            ],
            "connote" => [
                "connote_number" => 1,
                "connote_service" => "ECO",
                "connote_service_price"=> 70700,
                "connote_amount"=> 70700,
                "connote_code"=> "AWB00100209082020",
                "connote_booking_code"=> "",
                "connote_state"=> "PAID",
                "connote_state_id"=> 2,
                "surcharge_amount"=> null,
                "actual_weight"=> 20,
                "volume_weight"=> 0,
                "chargeable_weight"=> 20,
                "organization_id"=> 6,
                "location_id"=> "5cecb20b6c49615b174c3e74",
                "connote_total_package"=> "3",
                "connote_surcharge_amount"=> "0",
                "connote_sla_day"=> "4",
                "location_name"=> "Hub Jakarta Selatan",
                "location_type"=> "HUB",
                "source_tariff_db"=> "tariff_customers",
                "id_source_tariff"=> "1576868",
                "pod"=> null,
                "history"=> [],
                "koli_data" => [
                    [
                        "koli_length"=> 0,
                        "koli_chargeable_weight"=> 9,
                        "koli_width"=> 0,
                        "koli_surcharge"=> [],
                        "koli_height"=> 0,
                        "koli_description"=> "V WARP",
                        "koli_formula_id"=> null,
                        "koli_volume"=> 0,
                        "koli_weight"=> 9,
                        "koli_custom_field"=> [
                            "awb_sicepat"=> null,
                            "harga_barang"=> null
                        ]
                    ]
                ]
            ]
        ])->assertStatus(
            Response::HTTP_NOT_FOUND
        );
    }
    
    public function setUp() : void {
        parent::setUp();
        DB::connection('mongodb')->collection("customers")->insert([
            "_id" => new BSONObjectId("652e2c4919db60ea923c6c4d")
        ]);
        DB::connection('mongodb')->collection("customers")->insert([
            "_id" => new BSONObjectId("652e2c4919db60ea923c6c4e")
        ]);
        DB::connection('mongodb')->collection("customers")->insert([
            "_id" => new BSONObjectId("652e2cc719db60ea923c6c50")
        ]);
    }

    public function tearDown() : void {
        DB::connection('mongodb')->drop();
    }
}
