<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\ObjectId as BSONObjectId;
use Tests\TestCase;

class GetPackageTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_success_get_list_package(): void
    {
        $this->get('/api/v1/package?page=1&per_page=5')->assertJson([
            "data" => [
                [
                    "connote_code" => "AWB00100209082020",
                    "transaction" =>  [
                        "transaction_amount"=> "90700",
                        "transaction_code"=> "CGKFT202310191"
                    ],
                    "origin"=> [
                        "customer_name"=> "PT. NARA OKA PRAKARSA",
                        "customer_email"=> "info@naraoka.co.id"
                    ],
                    "destination"=> [
                        "customer_name"=> "PT. NARA OKA PRAKARSA",
                        "customer_email"=> "info@naraoka.co.id"
                    ]
                ]
            ],
            "meta" => [
                "page" => 1,
                "per_page" => 5,
                "total_data" => 1,
                "total_page" => 1
            ]
        ])->assertStatus(Response::HTTP_OK);
    }

    public function test_failed_get_list_package(): void {
        $this->get('/api/v1/package?page=satu&per_page=5')->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_failed_get_detail_package_not_found(): void {
        $this->get('/api/v1/package/11111')->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function setUp() : void {
        
        parent::setUp();

        DB::connection('mongodb')->collection("customers")->insert([
            "_id" => new BSONObjectId("652e2c4919db60ea923c6c4d"),
            "customer_name"=> "PT. NARA OKA PRAKARSA",
            "customer_address"=> "JL. KH. AHMAD DAHLAN NO. 100, SEMARANG TENGAH 12420",
            "customer_email"=> "info@naraoka.co.id",
            "customer_phone"=> "024-1234567",
            "customer_address_detail"=> null,
            "customer_zip_code"=> "12420",
            "zone_code"=> "CGKFT",
            "organization_id"=> 6,
            "location_id"=> "5cecb20b6c49615b174c3e74"
        ]);

        DB::connection('mongodb')->collection("transactions")->insert([
            "_id" => new BSONObjectId("6530dda8ad600d1d80093552"),
            "transaction_amount"=> "90700",
            "transaction_discount"=> "0",
            "transaction_additional_field"=> null,
            "transaction_payment_type"=> "29",
            "transaction_state"=> "PAID",
            "transaction_payment_type_name"=> "Invoice",
            "transaction_cash_amount"=> 0,
            "transaction_cash_change"=> 0,
            "origin_data" => new BSONObjectId("652e2c4919db60ea923c6c4d"),
            "destination_data"=> new BSONObjectId("652e2c4919db60ea923c6c4d"),
            "customer_id"=> new BSONObjectId("652e2c4919db60ea923c6c4d"),
            "transaction_id"=> "a7f695b3-6c84-4f26-8bba-212da1bd08d9",
            "transaction_code"=> "CGKFT202310191",
            "transaction_order"=> 1,
            "connote_id"=> "b97a665e-f32d-46e8-a871-8444dff42a5b"
        ]);

        DB::connection('mongodb')->collection("connotes")->insert([
            "_id" => new BSONObjectId("6530dda8ad600d1d80093553"),
            "connote_number"=> 1,
            "connote_service"=> "XOX",
            "connote_service_price"=> 70700,
            "connote_amount"=> 80700,
            "connote_code"=> "AWB00100209082020",
            "connote_booking_code"=> null,
            "connote_state"=> "PAID",
            "connote_state_id"=> 2,
            "surcharge_amount"=> null,
            "actual_weight"=> 20,
            "volume_weight"=> 0,
            "chargeable_weight"=> 20,
            "organization_id"=> 6,
            "connote_order"=> 1,
            "connote_id"=> "b97a665e-f32d-46e8-a871-8444dff42a5b"
        ]);
    }

    public function tearDown() : void {
        DB::connection('mongodb')->drop();
    }
}
