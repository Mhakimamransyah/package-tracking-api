<?php

use App\Http\Controllers\PackageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function(){
    Route::get("/package", [PackageController::class,'getAllPackages']);
    Route::get("/package/{id}", [PackageController::class,'getDetailPackage']);
    Route::post("/package", [PackageController::class,'createPackage']);
    Route::put("/package/{id}", [PackageController::class,'putPackage']);
    Route::patch("/package/{id}", [PackageController::class,'patchPackage']);
    Route::delete("/package/{id}", [PackageController::class,'deletePackage']);
});
