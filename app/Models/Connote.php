<?php

namespace App\Models;

use MongoDB\Laravel\Eloquent\Model;
use MongoDB\BSON\UTCDateTime;

class Connote extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function transaction(){
        return $this->belongsTo(
            Transaction::class,'connote_id','connote_id'
        );
    }

    public function koli(){
        return $this->hasMany(
            Koli::class,'connote_id','connote_id'
        );
    }

    public static function produce(array $request, int $order) : array {
        
        $connote                    = $request['connote'];
        $connote['zone_code_from']  = $request['origin']['zone_code'];
        $connote['zone_code_to']    = $request['destination']['zone_code'];
        $connote['connote_order']   = $order+1;

        unset($connote['koli_data']);

        if (isset($request['created_at'])) {
            $connote["created_at"] = new UTCDateTime(new \DateTime($request['created_at']));
        } elseif (isset($request['updated_at'])) {
            $connote["updated_at"] = new UTCDateTime(new \DateTime($request['updated_at']));
        }

        return $connote;
    }
}
