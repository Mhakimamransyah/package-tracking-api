<?php

namespace App\Models;

use MongoDB\Laravel\Eloquent\Model;
use MongoDB\BSON\ObjectId as BSONObjectId;
use MongoDB\BSON\UTCDateTime;

class Transaction extends Model
{
    
    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function origin(){
        return $this->hasOne(
            Customer::class,'_id','origin_data'
        );
    }

    public function destination(){
        return $this->hasOne(
            Customer::class,'_id','destination_data'
        );
    }

    public function customer(){
        return $this->hasOne(
            Customer::class,'_id','customer_id'
        );
    }

    public static function produce(array $request, int $order) : array {
        
        $transaction                        = $request['transaction'];
        $transaction["transaction_code"]    = $request['origin']["zone_code"].date('Ymd').($order+1);
        $transaction["transaction_order"]   = $order+1;
        $transaction["origin_data"]         = new BSONObjectId($request['transaction']['origin_data']);
        $transaction["destination_data"]    = new BSONObjectId($request['transaction']['destination_data']);
        $transaction["customer_id"]         = new BSONObjectId($request['transaction']['customer_id']);
        $transaction["connote_id"]          = $request['connote']['connote_id'];

        if (isset($request['created_at'])) {
            $transaction["created_at"] = new UTCDateTime(new \DateTime($request['created_at']));
        } elseif (isset($request['updated_at'])) {
            $transaction["updated_at"] = new UTCDateTime(new \DateTime($request['updated_at']));
        }
        
        return $transaction;
    }

}
