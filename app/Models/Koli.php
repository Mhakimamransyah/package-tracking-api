<?php

namespace App\Models;

use MongoDB\Laravel\Eloquent\Model;
use Illuminate\Support\Str;
use MongoDB\BSON\UTCDateTime;

class Koli extends Model
{
    const AWB_URL = "https:\/\/tracking.mile.app\/label\/%s";

    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public static function produce(array $request) : array {

        if(!$request['connote']['koli_data']) {
            return [];
        }
    
        $kolis = $request['connote']['koli_data'];
        
        foreach ($kolis as $key => &$koli) {

            $koli['awb_url']    = sprintf(self::AWB_URL, $request['connote']['connote_code'].".".($key+1));
            $koli["connote_id"] = $request['connote']['connote_id'];
            $koli["koli_id"]    = Str::uuid()->toString();
            $koli["koli_code"]  = $request['connote']['connote_code'].".".($key+1);
            
            if (isset($request['created_at'])) {
                $koli["created_at"] = new UTCDateTime(new \DateTime($request['created_at']));
            } elseif (isset($request['updated_at'])) {
                $koli["updated_at"] = new UTCDateTime(new \DateTime($request['updated_at']));
            }
        }

        return $kolis;
    }
}
