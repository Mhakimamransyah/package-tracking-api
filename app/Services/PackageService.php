<?php

namespace App\Services;
use App\Models\Customer;
use App\Models\Connote;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class PackageService {
    
    public function getAndValidateConnote( $id) : Connote {

        $existingConnote = Connote::where("connote_id","=",$id)
                            ->orWhere("connote_code","=",$id)->with([
                                "transaction.destination","transaction.origin","transaction.customer","koli"
                            ])->first();

        if($existingConnote == null) {
            throw new HttpResponseException(
                response([
                    "message" => Response::$statusTexts[Response::HTTP_NOT_FOUND],
                    "errors" => [
                        "id" => [
                            "Package not found"
                        ]
                    ]
                ],
                Response::HTTP_NOT_FOUND
                )
            );
        }

        return $existingConnote;
    }

    public function validateCustomer(array &$request) {
        
        $errorHandling = new \App\Exceptions\InvalidCustomer();

        if (isset($request['transaction']["origin_data"])) {
            
            $request["origin"] = Customer::where("_id",'=',$request['transaction']["origin_data"])->first();
            
            $errorHandling->put("origin", $request["origin"] ?? null);
        }

        if (isset($request['transaction']["destination_data"])) {
            
            $request["destination"]=Customer::where(
                "_id",'=',
                $request['transaction']["destination_data"]
            )->first();

            $errorHandling->put("destination", $request["destination"] ?? null);
        }

        if (isset($request['transaction']["customer_id"])) {
            
            $request["customer"] = Customer::where("_id",'=',$request['transaction']["customer_id"])->first();

            $errorHandling->put("customer", $request["customer"] ?? null);
        }

        $errorHandling->validate();
    }
}