<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Exception;

class InvalidCustomer extends Exception
{
    private $errors;

    public function __construct() {
        $this->errors = [];
    }

    public function put(string $type, $data) {
        if($data == null) {
            $this->errors[$type] = [$type." Not Found"];
        }
    }

    public function validate() {
        if(sizeof($this->errors) > 0) {
            throw $this;
        }
    }

    public function render()
    {
        return response()->json([
            'message' => "Customers not found",
            'errors' => $this->errors,
        ], Response::HTTP_NOT_FOUND);
    }
}
