<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Contracts\Validation\Validator;
use Exception;

class InvalidRequest extends Exception
{
    private Validator $validator;

    public function __construct(Validator $validator) {
        $this->validator = $validator;
    }

    public function render()
    {
        return response()
        ->json([
            'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
            'errors' => $this->validator->getMessageBag(),
        ], Response::HTTP_BAD_REQUEST);
    }
}
