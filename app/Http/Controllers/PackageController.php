<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListRequest;
use App\Services\PackageService;
use App\Http\Requests\package\DetailPackageRequest;
use App\Http\Requests\package\PackageRequest;
use App\Http\Resources\package\Packages;
use App\Http\Resources\package\Package;
use App\Models\Connote;
use App\Models\Koli;
use App\Models\Transaction;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PackageController extends Controller
{
    private PackageService $service;

    public function __construct(PackageService $service) {
        $this->service = $service;
    }

    public function getAllPackages(ListRequest $request): array {
        
        $request    = $request->validated();
        $connotes   = Connote::with('transaction');
        $perPage    = $request['per_page'] ?? 10;
        $page       = $request['page'] ?? 1;
        $totalRow   = $connotes->count();

        return [
            "data" => new Packages($connotes->limit($perPage)->offset(($page - 1) * $perPage)->get()),
            "meta" => [
                "page"       => (int)$page,
                "per_page"   => (int)$perPage,
                "total_data" => $totalRow,
                "total_page" => ceil(($totalRow)/$perPage)
            ]
        ];
        
    }

    public function getDetailPackage(DetailPackageRequest $request) : Package {

        $request = $request->validated();
        
        $existingConnote = $this->service->getAndValidateConnote($request["id"]);

        return new Package($existingConnote);
    }

    public function createPackage(PackageRequest $request) : Response {

        $request = $request->all();

        $this->service->validateCustomer($request);

        $request["connote"]["connote_id"]         = Str::uuid()->toString();
        $request["transaction"]["transaction_id"] = Str::uuid()->toString();

        DB::transaction(function () use (&$request) {

            $request['transaction_coll'] = Transaction::produce(
                $request, (Transaction::orderBy('transaction_order','DESC')->first()->transaction_order ?? 0)
            );
            $request['connote_coll'] = Connote::produce(
                $request, (Connote::orderBy('connote_order','DESC')->first()->connote_order ?? 0)
            );
            $request['koli_coll'] = Koli::produce(
                $request
            );

            Transaction::create($request['transaction_coll']);
            Connote::create($request['connote_coll']);
            Koli::insert($request['koli_coll']);

        });

        return new Response(
            [
                "message" => "Package Created",
                "data"    => [
                    "connote_id"      => $request["connote_coll"]['connote_id'],
                    "connote_code"    => $request["connote"]['connote_code'],
                    "transactions_id" => $request["transaction_coll"]['transaction_id']
                ]
            ],
            Response::HTTP_CREATED
        );
    }

    public function putPackage(PackageRequest $request) : Response {
        
        $request = $request->all();
        
        $this->service->validateCustomer($request);

        $existingConnote = $this->service->getAndValidateConnote($request["id"]);

        $request['connote']['connote_id'] = $existingConnote->connote_id;
        $request['transaction']['transaction_id'] = $existingConnote->transaction->transaction_id;
        
        DB::transaction(function () use (&$request,$existingConnote) {

            $transaction  = Transaction::produce($request, ($existingConnote->transaction->transaction_order-1));
            $connote      = Connote::produce($request, ($existingConnote->connote_order-1));
            $kolis        = Koli::produce($request);

            Transaction::Where("_id","=",$existingConnote->transaction->_id)->update($transaction);
            Connote::where("_id","=",$existingConnote->_id)->update($connote);
            Koli::where("connote_id","=",$existingConnote->connote_id)->delete();
            Koli::insert($kolis);

        });

        return new Response([
            "message" => "Package Replaced",
            "data"    => [
                "transaction"   => $request['transaction'],
                "connote"       => $request['connote']
            ],
        ]);
    }

    public function patchPackage(PackageRequest $request) : Response {
        
        $request = $request->all();
        $this->service->validateCustomer($request);
        $existingConnote = $this->service->getAndValidateConnote($request["id"]);

        DB::transaction(function () use ($request,$existingConnote) {

            if (isset($request['transaction'])) {
                Transaction::Where("_id","=",$existingConnote->transaction->_id)
                    ->update($request['transaction']);
            }

            if (isset($request['connote'])) {
                Connote::Where("_id","=",$existingConnote->_id)
                    ->update($request['connote']);
            }

        });

        return new Response([
            "message" => "Package attribute updated",
            "data"    => [
                "connote"       => $request['connote'],
                "transaction"   => $request['transaction'],
            ]
        ]);
    }

    public function deletePackage(DetailPackageRequest $request) : Response {
        
        $request = $request->validated();
        $existingConnote = $this->service->getAndValidateConnote($request["id"]);

        DB::transaction(function () use ($existingConnote) {
            Connote::Where("connote_id","=",$existingConnote->connote_id)->delete();
            Transaction::Where("connote_id","=",$existingConnote->connote_id)->delete();
            Koli::Where("connote_id","=",$existingConnote->connote_id)->delete();
        });

        return new Response([
            "message" => "package deleted",
            "data"    => [
                "connote_id"        => $existingConnote->connote_id,
                "connote_code"      => $existingConnote->connote_code,
                "transaction_id"    => $existingConnote->transaction->transaction_id
            ]
        ]);

    }

    
}
