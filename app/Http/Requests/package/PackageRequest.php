<?php

namespace App\Http\Requests\package;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function all($keys = null) : array {
        
        $data = parent::all();

        if ($this->route('id') != null) {
            $data['id'] = $this->route('id');
        }

        switch ($this->method()) {
            case Request::METHOD_PUT:
                $data['updated_at'] = date('Y-m-d H:i:s');
            break;
            case Request::METHOD_PATCH:
                $data['updated_at'] = date('Y-m-d H:i:s');
            break;
            case Request::METHOD_POST:
                $data['created_at'] = date('Y-m-d H:i:s');
            break;
            default: break;
        }

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        if (in_array($this->method(),[Request::METHOD_PUT,Request::METHOD_POST])) {
            return $this->putPackageRules();
        }

        return $this->patchPackageRules();
    }

    private function putPackageRules() : array {

        return [
            'transaction.transaction_amount'                    => 'numeric|required',
            'transaction.transaction_discount'                  => 'numeric',
            'transaction.transaction_payment_type'              => 'numeric|required',
            'transaction.transaction_payment_type_name'         => 'required',
            'transaction.transaction_cash_amount'               => 'numeric',
            'transaction.transaction_cash_change'               => 'numeric',
            'transaction.customer_attribute.Nama_Sales'         => 'required',
            'transaction.customer_attribute.TOP'                => 'required',
            'transaction.customer_attribute.Jenis_Pelanggan'    => 'required',
            'transaction.origin_data'                           => 'required',
            'transaction.destination_data'                      => 'required',
            'transaction.customer_id'                           => 'required',
            'connote.connote_number'                            => 'required|int',
            'connote.connote_service'                           => 'required|size:3',
            'connote.connote_service_price'                     => 'required|int',
            'connote.connote_service_price'                     => 'required|int',
            'connote.connote_state'                             => 'required',
            'connote.connote_state_id'                          => 'required|int',
            'connote.surcharge_amount'                          => 'nullable|int',
            'connote.actual_weight'                             => 'nullable|int',
            'connote.volume_weight'                             => 'nullable|int',
            'connote.organization_id'                           => 'required|int',
            'connote.connote_total_package'                     => 'required|int',
            'connote.connote_surcharge_amount'                  => 'nullable|int',
            'connote.connote_sla_day'                           => 'required|int',
            'connote.location_name'                             => 'required',
            'connote.location_type'                             => 'required',
            'connote.source_tariff_db'                          => 'required',
            'connote.id_source_tariff'                          => 'required|int',
            'connote.koli_data.*.koli_length'                   => 'required|int',
            'connote.koli_data.*.koli_width'                    => 'required|int',
            'connote.koli_data.*.koli_volume'                   => 'nullable|int',
            'connote.koli_data.*.koli_weight'                   => 'nullable|int'
        ];
    }

    private function patchPackageRules() : array {

        return [
            'transaction.transaction_amount'                    => 'numeric',
            'transaction.transaction_discount'                  => 'numeric',
            'transaction.transaction_payment_type'              => 'numeric',
            'transaction.transaction_cash_amount'               => 'numeric',
            'transaction.transaction_cash_change'               => 'numeric',
            'connote.connote_number'                            => 'nullable|int',
            'connote.connote_service'                           => 'size:3',
            'connote.connote_service_price'                     => 'nullable|int',
            'connote.connote_service_price'                     => 'nullable|int',
            'connote.connote_state_id'                          => 'nullable|int',
            'connote.surcharge_amount'                          => 'nullable|int',
            'connote.actual_weight'                             => 'nullable|int',
            'connote.volume_weight'                             => 'nullable|int',
            'connote.organization_id'                           => 'nullable|int',
            'connote.connote_total_package'                     => 'nullable|int',
            'connote.connote_surcharge_amount'                  => 'nullable|int',
            'connote.connote_sla_day'                           => 'nullable|int',
            'connote.id_source_tariff'                          => 'nullable|int'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new \App\Exceptions\InvalidRequest($validator);
    }
}
