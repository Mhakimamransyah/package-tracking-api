<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Pagination extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "data" => $this->resource,
            "meta" => [
                "page"          => $request['page'],
                "per_page"      => $request['per_page'],
                "total_rows"    => $request['total_rows'],
                "total_pages"   => $request['total_pages']
            ]
        ];
    }
}
