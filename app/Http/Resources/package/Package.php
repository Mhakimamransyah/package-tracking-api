<?php

namespace App\Http\Resources\package;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Package extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $this->resource->makeHidden([
            "_id"
        ]);

        optional($this->resource->transaction)->makeHidden([
            "_id","origin_data","destination_data","customer_id","connote_id"
        ]);

        return parent::toArray($request);
    }
}
