<?php

namespace App\Http\Resources\package;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class Packages extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->collection->map(
        
            function($package){
    
                return collect($package->toArray())->only([
                    
                    "connote_code"

                ])->merge(

                    [
                        "transaction" => collect($package->transaction)->only([
                            "transaction_code",
                            "transaction_amount",
                        ])->all()
                    ]

                )->merge(

                    [
                        "origin" =>  collect($package->transaction->origin)->only([
                            "customer_name",
                            "customer_email"
                        ])->all()
                    ]
                )->merge(
                    [
                        "destination" =>  collect($package->transaction->destination)->only([
                            "customer_name",
                            "customer_email"
                        ])->all()
                    ]
                )->all();
                
            })->toArray();
    }
}
