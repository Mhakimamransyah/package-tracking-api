# Package API Service
## 💻Tech. Stack
[![My Skills](https://skillicons.dev/icons?i=laravel,mongodb&theme=light)](https://skillicons.dev )
## 🦿Spesification
- Get all existing packages
- Get detail package
- Create new package
- Update package
- Delete package
## ✍🏼 OpenAPI Documentation
- Read online docs [here](https://app.swaggerhub.com/apis-docs/Mhakimamransyah/package-api/0.0.1#/default/get_package) .
- Download [here](https://gitlab.com/Mhakimamransyah/package-tracking-api/-/raw/master/documentation/doc.json?ref_type=heads&inline=false)
## ⚙️ Usage
### Define server environtment
Please add .env file to your local development machine. 
```
// .env.example
APP_NAME  = PackageApi
APP_ENV   = local
APP_KEY   = base64:i2BeYLtjt6kyLDHS4Wr1UNPrBvtW6t7g3tsJlb8NZzU=
APP_DEBUG = true
APP_URL   = http://localhost

MONGO_URI    = "mongodb+srv://username:password@domain.mongodb.net/"
MONGO_DBNAME = "dev"
```
### Seed customers data
Seed customers data using this artisan command
```
php artisan db:seed --class=CustomerSeeder 
```
### Run PHP local development server
Run local development server using this artisan command (port 8000)
```
php artisan serve
```
or define port by yours
```
php -S 127.0.0.1:8001 -t public
```
## 🧱 Testing
### Define environtment for testing
Please add .env.testing file to your local development machine.
```
// .env.testing.example
MONGO_URI    = "mongodb+srv://username:password@domain.mongodb.net/"
MONGO_DBNAME = "dev"
```
### Run testing
Run feature testing using this artisan command
```
php artisan test
```
